[Website][website]
===

_Providing a visual index of current (and past) interests._

License
---
I have licensed this project under [CC BY-NC 4.0][license].

You may negociate a waiver for the non-commercial restriction.
Please contact me at legal@projectaxil.us to do so.

[website]: https://emmanuel.website/
[license]: https://creativecommons.org/licenses/by-nc/4.0/
